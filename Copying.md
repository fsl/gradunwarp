# Copyright and Licenses

This is the [FSL gradunwarp package][fsl-gradunwarp].

It is forked from the [Human Connectome Project gradunwarp
package][gradunwarp-hcp], which itself is forked from a ["no longer actively
maintained" gradunwarp package][gradunwarp-ksubramz].

This fork contains changes made for the UK BioBank processing pipeline.

The HCP fork contains changes made for and by the WU-Minn Human Connectome
Project consortium ([HCP][HCP]).

This package, including all examples, code snippets and attached documentation
is covered by the MIT license (as supplied below.)

# 3rd Party Code

## NiBabel

The nibabel package, including all examples, code snippets and attached
documentation is covered by the MIT license.

# The MIT License

  * Copyright (c) 2012-2014 [WU-Minn Human Connectome Project consortium][HCP]
  * Copyright (c) 2009-2011 Matthew Brett <matthew.brett@gmail.com>
  * Copyright (c) 2010-2011 Stephan Gerhard <git@unidesign.ch>
  * Copyright (c) 2006-2010 Michael Hanke <michael.hanke@gmail.com>
  * Copyright (c) 2010-2011 Jarrod Millman <jarrod.millman@gmail.com>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

<!-- References -->

[fsl-gradunwarp]: https://git.fmrib.ox.ac.uk/fsl/gradunwarp
[gradunwarp-hcp]: https://github.com/Washington-University/gradunwarp
[gradunwarp-ksubramz]: https://github.com/ksubramz/gradunwarp
[HCP]: http://www.humanconnectome.org
